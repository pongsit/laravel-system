<?php

return [
    'google' => [
        'client_id'     => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect'      => env('GOOGLE_REDIRECT'),
    ],
    'line' => [    
        'client_id' => env('LINE_CLIENT_ID'),  
        'client_secret' => env('LINE_CLIENT_SECRET'),  
        'redirect' => env('LINE_REDIRECT'),
    ],
    'facebook' => [    
        'client_id' => env('FACEBOOK_CLIENT_ID'),  
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),  
        'redirect' => env('FACEBOOK_REDIRECT'),
    ]
];

// return [
// 	'version' => env('FIREBASE_VERSION'), // firebase version
// 	'apiKey' => env('FIREBASE_API_KEY'),
// 	'authDomain' => env('FIREBASE_AUTH_DOMAIN'),
// 	'projectId' => env('FIREBASE_PROJECT_ID'),
// 	'storageBucket' => env('FIREBASE_STORAGE_BUCKET'),
// 	'messagingSenderId' => env('FIREBASE_MESSAGING_SENDER_ID'),
// 	'appId' => env('FIREBASE_APP_ID'),
// 	'measurementId' => env('FIREBASE_MEASUREMENT_ID')
// ];
