<!DOCTYPE html>
<html lang="en">
<head>
    <x:head />
    <style>
        body{
            height: 100vh;
            color: rgb(148 163 184);
            background-color: #0F1A29;
            background-image: radial-gradient(145.05% 100% at 50% 0%,#1D2B41 0%,#020509 57.38%,#0F1A29 88.16%);
        }
    </style>
</head>
<body>
    <x:header />
    <div class="container" >
    	<div class="row justify-content-center mb-3 pb-0 pt-3">
	        <div class="col-12 pb-0 mb-0 text-center d-flex justify-content-center">
	            <div class="col-3 col-lg-2 text-end d-flex align-items-center">
	                <img class="w-100" src="{{asset('img/section1.png')}}">
	            </div>
	            <div class="px-3" style="font-size: 36px;">
	                เข้าสู่ระบบ
	            </div>
	            <div class="col-3 col-lg-2 text-start d-flex align-items-center">
	                <img class="w-100"  src="{{asset('img/section2.png')}}">
	            </div>
	        </div>
	    </div>
        <div class="row justify-content-center align-items-center text-center">
		    <div class="col col-sm-8 col-md-7 col-lg-4" >
		        <form method="post" class="application">
		            @csrf
		            <div class="mb-2 mt-2">
		                <input type="text" required class="form-control me-2" style="background-color: #1D2B41; color:white;" placeholder="อีเมล" name="email" >
		            </div>
		            <div class="mb-2 mt-2">
			            <input type="password" required class="form-control me-2" style="background-color: #1D2B41; color:white;" placeholder="รหัสผ่าน" name="password" >
			        </div>
			        <div class="mb-2 mt-2">
			        	<input type="submit" class="btn border text-nowrap" style="bottom: 0;background-color: #1D2B41; color:white;" value="ส่งข้อมูล">
			        </div>
		        </form>
		    </div>
		</div>
    </div>
    <x:footer />
</body>
</html>