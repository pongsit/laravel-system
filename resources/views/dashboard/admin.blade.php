@extends('layouts.dashboard')

@section('content')
    <div class="d-flex flex-wrap justify-content-center justify-content-md-start">
        @if(user()->can('manage_user'))
        <div class="col-12 col-md-6 col-lg-4 px-2 pb-3">
            <a href="{{url('user')}}" class="text-white">
                <div style="background-color: rgb(33,37,41,0.6);position:relative; border: solid 2px grey; height: 100%; overflow:scroll;" class="col text-center rounded text-nowrap">
                    <div style="position:absolute; top:0px; left:15px;"> ยอดสมาชิก: </div>
                    <div class="d-flex align-items-center justify-content-center pt-3" style="overflow:scroll; font-size:45px; height: 100%;">{{$number_of_member ?? 0}}</div>
                </div>
            </a>
        </div>
        @endif
        @if(user()->can('manage_role'))
        <div class="col-12 col-md-6 col-lg-4 px-2 pb-3">
            <div class="card shadow" style="height:100%;">
                <div class="card-header pt-3 pb-3 d-flex justify-content-between">
                    <div><h6 class="m-0 font-weight-bold">บทบาท</h6></div>
                    <div><a href="{{route('role.index')}}" ><i class="fas fa-edit"></i> </a></div>
                </div>
                <div class="card-body">
                    <div class="d-flex">
                        {{$role_show_all ?? '' }}
                    </div>
                    <hr />
                    <div>
                        ความสามารถ <a href="{{route('role.ability')}}" >กำหนด</a>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
@stop

@section('foot')
<script>
    $(function(){
        $("body").on("mouseover",".card",function(){
            $(this).css("border","solid 2px white");
        });
        $("body").on("mouseout",".card",function(){
            $(this).css("border","solid 2px grey");
        });
    });
</script>
@stop