<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="description" content="{{$_ENV['SITE_DESCRIPTION']}}" />
<meta name="author" content="{{$_ENV['SITE_AUTHOR']}}" />
<title>{{$pageName ?? $_ENV['SITE_NAME']}}</title>

<!-- Favicon-->
<link rel="icon" type="image/png" href="{{asset('vendor/system/img/favicon.png')}}">
<link rel="apple-touch-icon" type="image/png" href="{{asset('vendor/system/img/favicon.png')}}">

<!-- Custom fonts for this template -->
<link href="{{asset('vendor/system/fontawesome/css/all.min.css')}}" rel="stylesheet" type="text/css">

<script src="{{ asset('vendor/system/jquery/jquery.min.js') }}"></script>