@props(['product'=>$product])

{{-- filepond --}}
{{-- add class filepond to input file type --}}
{{-- add create folder filepond in storage/app/filepond --}}
{{-- 
    // Get the temporary path using the serverId returned by the upload function in `FilepondController.php`
    $filepond = app(\Sopamo\LaravelFilepond\Filepond::class);
    $disk = config('filepond.temporary_files_disk');

    $path = $filepond->getPathFromServerId($serverId);
    $fullpath = Storage::disk($disk)->get($filePath);


    // Move the file from the temporary path to the final location
    $finalLocation = public_path('output.jpg');
    File::move($fullpath, $finalLocation);
 --}}
<input class="filepond" type="file" data-max-file-size="3MB" name="file">
<style>
    .filepond--credits{
        display: none;
    }
    body .filepond--root {
        font-family: THSarabun;
        font-size: 28px;
    }
    body .filepond--panel-root {
        background-color: rgba(33,37,41);
        border: 2px solid grey;
    }
    body .filepond--drop-label {
      color: white;
    }
</style>
<link href="{{url('/vendor/pongsit/system/filepond/filepond.css')}}" rel="stylesheet" />
<link href="{{url('/vendor/pongsit/system/filepond/filepond-plugin-image-edit.css')}}" rel="stylesheet" />
<link href="{{url('/vendor/pongsit/system/filepond/filepond-plugin-image-preview.css')}}" rel="stylesheet" />
<script src="{{url('/vendor/pongsit/system/filepond/filepond-plugin-image-preview.js')}}"></script>
<script src="{{url('/vendor/pongsit/system/filepond/filepond-plugin-file-validate-type.js')}}"></script>
<script src="{{url('/vendor/pongsit/system/filepond/filepond-plugin-image-transform.js')}}"></script>
<script src="{{url('/vendor/pongsit/system/filepond/filepond-plugin-image-exif-orientation.js')}}"></script>
<script src="{{url('/vendor/pongsit/system/filepond/filepond-plugin-image-validate-size.js')}}"></script>
<script src="{{url('/vendor/pongsit/system/filepond/filepond.js')}}"></script>

<script>
    var remove_lists = [];
    var error_lists = [];
    var error_list = '';
    var filename_to_ids = {};
    FilePond.registerPlugin(
      FilePondPluginImagePreview,
      FilePondPluginFileValidateType,
      FilePondPluginImageTransform,
      FilePondPluginImageExifOrientation,
      FilePondPluginImageValidateSize
    );
    FilePond.parse(document.body);
    FilePond.setOptions({
      imageValidateSizeMaxWidth:3000,
      imageValidateSizeMinWidth:450,
      maxFiles:7,
      instantUpload:true,
      allowBrowse: true,
      acceptedFileTypes:['image/png', 'image/jpeg'],
      allowReorder:true,
      allowMultiple:true,
      labelIdle:'อัพโหลดรูปที่นี่',
      onprocessfile:function(error, file){
      },
      onprocessfiles:function(){
        var files = fp.getFiles();
        if(remove_lists){
          $.each(files,function(k,v){
            if($.inArray(v.filename,remove_lists) > -1){
              fp.removeFile(v.id);
            }
          });
          if(error_list){
            Swal.fire({
                icon: 'error',
                title: 'ขออภัย',
                html: error_list
            });
            error_list = '';
            error_lists = [];
          }
        }
      },
      onprocessfilerevert:function(file){
       var posting = $.post("{{url('/product/remove/photo/'.$product->slug)}}",{'photo_name':file.filename,'_token':'{{ csrf_token() }}'});
        posting.done(function(data){
          if(data){
            if(data.error){
              if($.inArray(data.error,error_lists) == -1){
                error_lists.push(data.error);
                error_list += data.error+'<br>';
              }
            }
          }
        });
      },
      onaddfile:function(error, file){
      },
      onupdatefiles:function(files){
      },
      onremovefile:function(error, file){
        var posting = $.post("{{url('/product/remove/photo/'.$product->slug)}}",{'photo_name':file.filename,'_token':'{{ csrf_token() }}'});
        posting.done(function(data){
          if(data){
            if(data.error){
              if($.inArray(data.error,error_lists) == -1){
                error_lists.push(data.error);
                error_list += data.error+'<br>';
              }
            }
          }
        });
      },
      oninitfile:function(file){
        // console.log(file);
      },
      onreorderfiles:function(files, origin, target){
        var photos = [];
        $.each(files,function(k,v){
            photos.push(v.filename);
        });
        var posting = $.post("{{url('/product/reorder/photo/'.$product->slug)}}",{'photos':photos,'_token':'{{ csrf_token() }}'});
        posting.done(function(data){
          console.log(data);
          if(data){
            if(data.error){
              if($.inArray(data.error,error_lists) == -1){
                error_lists.push(data.error);
                error_list += data.error+'<br>';
              }
            }
          }
        });
      }
    });
    var fp = FilePond.create(
      document.querySelector('.filepond'),
      {
        name: 'file',
        files: [
          @foreach($product->photos()->orderBy('slot')->get() as $v)
            {
              // the server file reference
              source: '{{$product->id}}-{{$v->name}}-md',
              // set type to local to indicate an already uploaded file
              options: {
                type: 'local',
              }
            },
          @endforeach
        ],
        server: {
          url: "{{url('/filepond/api')}}",
          revert: '/revert',
          patch: "?patch=",
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          process: {
              url: "/process",
              method: 'POST',
              onload: (serverId) => {
                var files = fp.getFiles();
                var photos = [];
                $.each(files,function(k,v){          
                  photos.push(v.filename);
                });
                var posting = $.post("{{url('/product/store/photo/'.$product->slug)}}", {'photos':photos,'serverId':serverId,'_token':'{{ csrf_token() }}'});
                posting.done(function(data){
                  console.log(data);
                  if(data){
                    if(data.success){
                    }
                    if(data.error){
                      remove_lists.push(data.name);
                      if($.inArray(data.error,error_lists) == -1){
                        error_lists.push(data.error);
                        error_list += data.error+'<br>';
                      }
                    }
                  }
                });

                // response = JSON.parse(response);
                // console.log(response);
                // return response.key;
              },
              // onerror: (response) => {
              //   console.log("raw", response)
              //   response = JSON.parse(response);
              //   console.log(response);
              //   return response.msg
              // },
              // ondata: (formData) => {
              //   window.h = formData;
              //   console.log(formData)
              //   return formData;
              // }
          },
          load: '/load/'
        }
      }
    );
</script>



{{-- exmple --}}
{{-- 
 window.addEventListener("DOMContentLoaded", function () {
      // initializing file pond js 
      FilePond.registerPlugin(
        FilePondPluginImagePreview,
        FilePondPluginImageExifOrientation,
        FilePondPluginFileValidateSize,
        FilePondPluginImageEdit,
        FilePondPluginFileValidateType
      );

      // Select the file input and use 
      // create() to turn it into a pond
      FilePond.create(
        document.querySelector('#imagesFilepond'),
        {
          name: 'filepond',
          maxFiles: 10, // maximum files to upload
          allowBrowse: true,
          acceptedFileTypes: ['image/*'], // file types to accept (here images/only)
          // server
          server: {
            load: (uniqueFileId, load, error, progress, abort, headers) => {
              console.log('attempting to load', uniqueFileId);
              // implement logic to load file from server here
              // https://pqina.nl/filepond/docs/patterns/api/server/#load-1

              let controller = new AbortController();
              let signal = controller.signal;

              fetch(`load.php?key=${uniqueFileId}`, {
                method: "GET",
                signal,
              })
                .then(res => {
                  window.c = res
                  console.log(res)
                  return res.blob()
                })
                .then(blob => {
                  console.log(blob)
                  // const imageFileObj = new File([blob], `${uniqueFileId}.${blob.type.split('/')[1]}`, {
                  //   type: blob.type
                  // }) 
                  // console.log(imageFileObj)
                  // progress(true, size, size);
                  load(blob);
                })
                .catch(err => {
                  console.log(err)
                  error(err.message);
                })

              return {
                abort: () => {
                  // User tapped cancel, abort our ongoing actions here
                  controller.abort();
                  // Let FilePond know the request has been cancelled
                  abort();
                }
              };
            },
            // remove: 
          },
          //files array
          files: [
            // display existing campaign images from the server here
            {
              // the server file reference
              source: '6150c75710bd9',
              // set type to local to indicate an already uploaded file
              options: {
                type: 'local',
              }
            },
            {
              // the server file reference
              source: '6150c3c865bd3',
              // set type to local to indicate an already uploaded file
              options: {
                type: 'local',
              }
            },
          ],
        }
      );

      FilePond.setOptions({
        server: {
          // url: "/",
          process: {
            url: 'process.php',
            method: 'POST',
            headers: {
              'x-customheader': 'Processing File'
            },
            onload: (response) => {
              console.log("raw", response)
              response = JSON.parse(response);
              console.log(response);
              return response.key;
            },
            onerror: (response) => {
              console.log("raw", response)
              response = JSON.parse(response);
              console.log(response);
              return response.msg
            },
            ondata: (formData) => {
              window.h = formData;
              console.log(formData)
              return formData;
            }
          },
          revert: (uniqueFileId, load, error) => {
            const formData = new FormData();
            formData.append("key", uniqueFileId);

            console.log(uniqueFileId);

            fetch(`revert.php?key=${uniqueFileId}`, {
              method: "DELETE",
              body: formData,
            })
              .then(res => res.json())
              .then(json => {
                console.log(json);
                if (json.status == "success") {
                  // Should call the load method when done, no parameters required
                  load();
                } else {
                  // Can call the error method if something is wrong, should exit after
                  error(err.msg);
                }
              })
              .catch(err => {
                console.log(err)
                // Can call the error method if something is wrong, should exit after
                error(err.message);
              })
          },
          remove: (uniqueFileId, load, error) => {
            const formData = new FormData();
            formData.append("key", uniqueFileId);

            console.log(uniqueFileId);

            fetch(`revert.php?key=${uniqueFileId}`, {
              method: "DELETE",
              body: formData,
            })
              .then(res => res.json())
              .then(json => {
                console.log(json);
                if (json.status == "success") {
                  // Should call the load method when done, no parameters required
                  load();
                } else {
                  // Can call the error method if something is wrong, should exit after
                  error(err.msg);
                }
              })
              .catch(err => {
                console.log(err)
                // Can call the error method if something is wrong, should exit after
                error(err.message);
              })
          },
          restore: (uniqueFileId, load, error, progress, abort, headers) => {
            let controller = new AbortController();
            let signal = controller.signal;

            fetch(`load.php?key=${uniqueFileId}`, {
              method: "GET",
              signal,
            })
              .then(res => {
                window.c = res
                console.log(res)
                const headers = res.headers;
                const contentLength = +headers.get("content-length");
                const contentDisposition = headers.get("content-disposition");
                let fileName = contentDisposition.split("filename=")[1];
                fileName = fileName.slice(1, fileName.length - 1)
                progress(true, contentLength, contentLength);
                return {
                  blob: res.blob(),
                  size: contentLength,
                }
              })
              .then(({ blob, size }) => {
                console.log(blob)
                // headersString = 'Content-Disposition: inline; filename="my-file.jpg"'
                // headers(headersString);

                const imageFileObj = new File([blob], `${uniqueFileId}.${blob.type.split('/')[1]}`, {
                  type: blob.type
                })
                console.log(imageFileObj)
                progress(true, size, size);
                load(imageFileObj);
              })
              .catch(err => {
                console.log(err)
                error(err.message);
              })

            return {
              abort: () => {
                // User tapped cancel, abort our ongoing actions here
                controller.abort();
                // Let FilePond know the request has been cancelled
                abort();
              }
            };
          },
        },
      })
    })
   --}}