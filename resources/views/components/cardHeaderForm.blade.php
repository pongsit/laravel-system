<h6 class="m-0 font-weight-bold">{{$cardTitle}}</h6>
<div>
    <a class="{{$cardName}}-edit" href="javascript:;"><i style="font-size:0.7em;" class="fas fa-edit"></i> แก้ไข</a>
    <a class="btn btn-success {{$cardName}}-edit-ok {{$cardName}}-edit-form" style="display: none;" href="javascript:;">ตกลง</a>
    <a class="btn btn-danger {{$cardName}}-edit-cancel {{$cardName}}-edit-form" style="display: none;" href="javascript:;">ยกเลิก</a>
    <script>
        $(function(){
            $('body').on('click tap','.{{$cardName}}-edit',function(){
                $('.{{$cardName}}-edit').hide();
                $('.{{$cardName}}-edit-show').hide();
                $('.{{$cardName}}-edit-form').show();
            });

            $('body').on('click tap','.{{$cardName}}-edit-ok',function(){
                $('.{{$cardName}}-edit').show();
                $('.{{$cardName}}-edit-show').show();
                $('.{{$cardName}}-edit-form').hide();
                $('#{{$cardName}}-edit-form').submit();
            });
            
            $('body').on('click tap','.{{$cardName}}-edit-cancel',function(){
                $('.{{$cardName}}-edit').show();
                $('.{{$cardName}}-edit-show').show();
                $('.{{$cardName}}-edit-form').hide();
            });
        });
    </script>
</div>
    