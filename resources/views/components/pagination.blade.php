@if(!(empty($next_page_url) && empty($prev_page_url)))
<div class="row mt-3">
    <div class="col">
        <div class="justify-content-end" style="overflow:scroll;">
            <nav>
                <ul class="pagination">
                    @if($prev_page_url)
                    <li class="page-item">
                        <a class="dark page-link text-nowrap" 
                            href="{{$prev_page_url}}{{$append_query_string}}" tabindex="-1">ก่อนหน้า</a>
                    </li>
                    @endif
                    @foreach($links as $k=>$vs)
                        @php
                            if($k == 0){ continue; }
                            if($k == count($links)-1){ continue; }
                        @endphp
                        <li class="page-item @if($vs['active']) active @endif">
                            <a class="@if(!$vs['active']) dark @endif page-link" 
                                href="{{$vs['url']}}{{$append_query_string}}">{{$vs['label']}}</a>
                        </li>
                    @endforeach
                    @if($next_page_url)
                    <li class="page-item">
                        <a class="dark page-link text-nowrap" 
                            href="{{$next_page_url}}{{$append_query_string}}">ถัดไป</a>
                    </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</div>
@endif