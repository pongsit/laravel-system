@props(['user'=>$user])
{{-- croppie --}}
<link rel="stylesheet" href="{{url('/vendor/pongsit/system/croppie/croppie.min.css')}}" />
<script src="{{url('/vendor/pongsit/system/croppie/croppie.min.js')}}"></script>

<div>
    <div class="row">
        <div class="col-md-12 text-center mt-3">
            <h4 class="text-center mt-3 mb-3">เปลี่ยนรูป</h4>
            <div id="upload-prview"></div>
        </div>
        
        <div class="col-md-12 text-center">
            <label class="m-0 btn btn-primary" style="display: inline-block;">
                <span>เลือกรูป</span>
                <input type="file" id="image_file" class="d-none">
            </label>
            <button class="btn btn-primary upload-image" id="uploadButton">Upload Image</button>
        </div>
        <div id="preview-crop-image" style="background:#9d9d9d;width:300px;padding:50px 50px;height:300px; display: none;"></div>
    </div>
</div>
<form id="croppie" class="d-none" method="post" action="{{url('/profile/update/avatar')}}/{{$user->slug}}" enctype="multipart/form-data">
  <input id="avatar" type="file" name="avatar">
  @csrf
</form>
<script>
    $('#uploadButton').hide();

    var resize = $('#upload-prview').croppie({
        enableExif: true,
        enableOrientation: true,    
        viewport: {
            width: 250,
            height: 250,
            type: 'square',
        },
        boundary: {
            width: 280,
            height: 280
        }
    });

    $('#image_file').on('change', function () { 
      var reader = new FileReader();
      reader.onload = function (e) {
        resize.croppie('bind',{
          url: e.target.result
        }).then(function(){
          $('#uploadButton').show();
        });
      }
      reader.readAsDataURL(this.files[0]);
    });
    $('.upload-image').on('click', function (ev) {
        resize.croppie('result', {
          type: 'blob',
          size: "viewport",
          quality : 1
        }).then(function (img) {
          let fileInputElement = document.getElementById('avatar');
          let container = new DataTransfer();
          let file = new File([img], "img.png",{type:"image/png", lastModified:new Date().getTime()});
          container.items.add(file);
          fileInputElement.files = container.files;
          $('#croppie').submit();
        });
    });
</script>