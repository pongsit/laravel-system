<script src="{{ asset('vendor/pongsit/system/sweetalert2/sweetalert2.all.min.js') }}"></script>
@if ($errors->any() || session()->has('error') || session()->has('success'))
<script>
    $(function(){
        var error = "{{ session()->get('error') }}";
        var and = '';
        @foreach ($errors->all() as $v)
            error += and+'{{$v}}';
            and = 'และ';
        @endforeach
        if(error){
            Swal.fire({
                icon: 'error',
                title: 'ขออภัย',
                text: error
            })
        }
        var success = "{{ session()->get('success') }}";
        if(success){
            Swal.fire({
                icon: 'success',
                title: 'สำเร็จ',
                text: success,
                showConfirmButton: false,
                timer: 2000
            })
        }
    });
</script>
    {{-- <div id="system-error" class="d-none">
        hi
        @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
        @endforeach
        @if(session()->has('error'))
            {{ session()->get('error') }}
        @endif
    </div>
    <div id="system-success" class="d-none">
        @if(session()->has('success'))
            {{ session()->get('success') }}
        @endif
    </div> --}}
@endif