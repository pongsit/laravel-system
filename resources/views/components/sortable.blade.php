{{-- sortable --}}
<script src="{{url('/vendor/pongsit/system/sortable/sortable.js')}}"></script>
<script>
  $(function(){
    // List with handle
    Sortable.create(listWithHandle, {
      handle: '.glyphicon-move',
      animation: 150
    });
  });
</script>

<!-- List with handle -->
<div id="listWithHandle" class="list-group">
  <div class="list-group-item">
    <span class="badge">14</span>
    <span class="glyphicon-move"><i class="fas fa-edit"></i></span>
    Drag me by the handle
  </div>
  <div class="list-group-item">
    <span class="badge">2</span>
    <span class="glyphicon-move"><i class="fas fa-edit"></i></span>
    You can also select text
  </div>
  <div class="list-group-item">
    <span class="badge">1</span>
    <span class="glyphicon-move"><i class="fas fa-edit"></i></span>
    Best of both worlds!
  </div>
</div>
