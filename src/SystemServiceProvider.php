<?php

// php artisan vendor:publish --provider="Pongsit\Firebase\FirebaseServiceProvider"

namespace Pongsit\System;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Pongsit\System\Providers\EventServiceProvider;
use Pongsit\System\Http\Middleware\IsLogin;

class SystemServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/services.php', 'services'
        );

        $this->app->register(EventServiceProvider::class); // For line socialite driver
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->publishes([
        //     __DIR__.'/../config/hi.php' => config_path('hi.php'),
        // ]);

        // $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->app['router']->namespace('Pongsit\\System\\Controllers')
                ->middleware(['web'])
                ->group(function () {
                    $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
                });
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'system');
        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/pongsit/system'),
            __DIR__.'/../config/system.php' => config_path('system.php'),
        ], 'public');
        $this->app['router']->aliasMiddleware('isLogin', IsLogin::class);
        // $this->app['router']->aliasMiddleware('own', Own::class);

        // $router = $this->app->make(Router::class);
        // $router->aliasMiddleware('firebase.auth', FirbaseAuth::class);
    }
}