<?php

namespace Pongsit\System\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!empty(user()->id)){
            return $next($request);
        }
        return back()->with(['error'=>'คุณไม่มีสิทธิ์เข้าใช้หน้านี้ครับ']);
    }
}