<?php

namespace Pongsit\System\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Pongsit\User\Models\User;
use Cookie;
use Laravel\Socialite\Facades\Socialite;
use Pongsit\Role\Models\Role;
use Pongsit\System\Models\System;
use Pongsit\Ability\Models\Ability;
use Throwable;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

use Illuminate\Support\Facades\Url;
use Firebase\JWT\JWT;
Use Firebase\JWT\Key;
use Illuminate\Support\Str;

class SystemController extends Controller
{

    public function index(){
		return view('index');
	}

	public function register(){
		return view('index',$infos);
	}

	public function login(){
		if(!user()->isLogin()){
			if(view()->exists('login')){
				return view('login');
			}else{
				return view('system::login');
			}
		}else{
			return redirect('/')->with(['success'=>'ยินดีต้อนรับเข้าสู่ระบบ']);
		}
	}

	public function loginFormPost(Request $request){
		$inputs = $request->all();
		$remember_me = 1;

        $request->validate([
			'password' => 'required',
			'email' => [
		        'required',
		        'email'
		    ],
		],[
			'password.required'=>'กรุณากรอกรหัสผ่าน',
			'email.required'=>'กรุณากรอกอีเมล',
			'email.email'=>'อีเมลนี้ไม่สามารถใช้ได้',
		]);

		if(auth()->attempt(array('email' => $inputs['email'], 'password' => $inputs['password'], 'status'=>1 ), $remember_me)){
			$auth_user = auth()->user();
            Auth::login($auth_user,true);
			$user = user($auth_user->id);
			session(['user' => $user]);
			return redirect('/')->with(['success'=>'ยินดีต้อนรับเข้าสู่ระบบ']);
		}else{
			return back()->with(['error'=>'อีเมลหรือรหัสผ่านไม่ถูกต้อง']);
		}
	}

	public function loginForm(){
		if(!user()->isLogin()){
			return view('login.form');
		}else{
			return redirect('/')->with(['success'=>'ยินดีต้อนรับเข้าสู่ระบบ']);
		}
	}

	public function logout(Request $request){
		Auth::logout();
		$request->session()->flush();

		// return redirect('/')->with(['success'=>'ออกจากระบบเรียบร้อย']);
		return view('system::logout');
	}

	public function logoutSuccess(){
		return redirect('/')->with(['success'=>'ออกจากระบบเรียบร้อย']);
	}
	public function logoutError(){
		return redirect('/')->with(['error'=>'ไม่สามารถอออกจากระบบได้กรุณาลองใหม่']);
	}

	public function getSocialiteProvider($provider){
		return Socialite::driver($provider)->redirect();
	}

	public function getSocialiteCallback($provider) {
		$user = Socialite::driver($provider)->user();
		if(!empty($user)){
			$findingUser = User::where($provider.'_id', $user->id)->first();
			if (Auth::user()){
				$authUser = Auth::user();
				$authUser[$provider.'_id'] = $user->id;
				$authUser->save();
				return redirect('/')->with(['success'=>'ยินดีต้อนรับเข้าสู่ระบบ']);
			}else{
				if($findingUser){
					Auth::login($findingUser,true);
					return redirect('/')->with(['success'=>'ยินดีต้อนรับเข้าสู่ระบบ']);
				}else{
					if(empty($user->email)){
						if(!empty(env('DOMAIN'))){
							$user->email = date('YmdHis').env('DOMAIN');
						}else{
							dd('Please set the domain in env.');
						}
					}   
					try{
						$newUser = Usercreate([
							'name' => $user->name,
							'email' => $user->email,
							'password' => bcrypt(Str::random(10)),
							// 'remember_token' => $user->token
						]);
						$newUser[$provider.'_id'] = $user->id;
						$newUser->save();
					}catch(\Throwable $e){
						// $errorCode = $e->errorInfo[1];
						// if($errorCode == 1062){
							return redirect()->route('login')->with(['error'=>'มีอีเมลนี้อยู่ในระบบแล้วกรุณาเลือกใช้อีเมลใหม่ หรือหากคุณเป็นเจ้าของกรุณา Login และเชื่อมต่อ Social อีกครั้ง']);
							// dd('Duplicate! If you own the account, please sign in.');
							// duplicate entry problem
							// return redirect('/message/duplicate');
						// }
					}
					Auth::login($newUser,true);
					return redirect('/')->with(['success'=>'ยินดีต้อนรับเข้าสู่ระบบ']);
				}
			}
		}
	}

    public function dashboardAdmin(User $user){

        if(!user()->isLogin()){
            return redirect()->route('login');
        }

        if(!user()->can('view_admin_dashboard')){
        	return redirect('/')->with(['error'=>'คุณไม่มีสิทธิ์เข้าใช้หน้านี้ครับ']);
        }

        $variables['user'] = $user;

        if(user()->can('manage_user')){
        	$variables['number_of_member'] = User::where('status',1)->count();
        }

        $all_roles = role()->where('status',1)->orderBy('power','desc')->get();

       	if(user()->can('manage_role')){
	        $variables['role_show_all'] = '';
	        $comma = '';
	        foreach($all_roles as $vs){
	        	$variables['role_show_all'] .= $comma.$vs->name_show;
	        	$comma = ', ';
	        }
	    }

	 //    $ability = new Ability();
		// $abilities = $ability->get();
		// $variables['ability_checks'] = array();
		// if(!empty($abilities)){
		// 	foreach($abilities as $v){
		// 		if(!empty($variables['roles'])){
		// 			foreach($variables['roles'] as $__v){
		// 				$variables['ability_checks'][$v->id][$__v->id] = 0;
		// 			}
		// 			foreach($v->roles()->get() as $_v){
		// 				$variables['ability_checks'][$v->id][$_v->id] = 1;
		// 			}
		// 		}
		// 	}
		// }
		// $variables['abilities'] = $abilities;

	 //    if(user()->can('manage_ability')){
	 //        $variables['abilities'] = '';
	 //        $comma = '';
	 //        foreach($all_roles as $vs){
	 //        	$variables['abilities'] .= $comma.$vs->name_show;
	 //        	$comma = ', ';
	 //        }
	 //    }



		// if(user()->can('manage_user') && !empty($user->id)){
  //   		$variables['user'] = $user;
  //   	}else{
  //   		$variables['user'] = user();	
  //   	}

    	// $variables['own'] = 0;
    	// if(user()->id == $variables['user']->id){
    	// 	$variables['own'] = 1;
    	// }

    	// if(user()->can('see_admin_dashboard') && user()->id == $variables['user']->id){
    	// 	$variables['number_of_member'] = User::where('status',1)->count();
     //        $variables['role_show_all'] = '';
     //        $comma = '';
     //        foreach(role()->getAll() as $vs){
     //        	$variables['role_show_all'] .= $comma.$vs->name_show;
     //        	$comma = ', ';
     //        }
     //        $variables['payment_remain'] = number_format(payment()->remain(),2);
     //        $variables['user_payment'] = payment()->getNumberOfUser();
     //        return view('dashboardAdmin',$variables);
    	// }
    	
    	// $variables = $infos;
    	
    	if(view()->exists('dashboard.admin')){
			return view('dashboard.admin',$variables);
		}else{
			return view('system::dashboard.admin',$variables);
		}
    }

}
