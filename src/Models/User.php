<?php

namespace Pongsit\System\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Pongsit\Profile\Models\Profile;
use Pongsit\Role\Models\Role;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Str;

class User extends Model
{
    use HasFactory;
    // public $infos = array();

    // Disable Laravel's mass assignment protection
    protected $guarded = [];

    /**
     * Boot the model.
     */
    protected static function boot()
    {
        parent::boot();

        // static::created(function ($user) {
        //     $user->slug = $this->createSlug($user->name);
        //     $user->save();
        // });
    }

   //  public function createSlug($name){
   //      if (static::whereSlug($slug = Str::slug($name))->exists()) {


   //       $max = static::whereName($name)->latest('id')->skip(1)->value('slug');


   //       if (is_numeric($max[-1])) {

   //          return preg_replace_callback('/(\d+)$/', function ($mathces) {

   //             return $mathces[1] + 1;

   //          }, $max);

   //       }
   //       return "{$slug}-2";
   //    }
   //    return $slug;
   // }
    
    public function roles() {
        return $this->belongsToMany('Pongsit\Role\Models\Role')->withTimestamps();
    }

    public function payments() {
        return $this->hasMany('Pongsit\Payment\Models\Payment');
    }
    
    public function profiles() {
        return $this->hasOne('Pongsit\Profile\Models\Profile');
    }

    public function getPayment(){
        if(empty($this->infos['id'])){
            $user = Auth::user();
            if(empty($user)){
                return [];
            }
            $user_id = $user->id;
        }else{
            $user_id = $this->infos['id'];
        }

        $user = $this->with('payments')->find($user_id);
        return $user->payments->toArray();
    }

    public function getInfosAttribute(){
        return $this->getInfo();
    }

    public function getInfoAttribute(){
        return (object) $this->getInfo();
    }

    // public function getAvatarAttribute(){
    //     return $this->avatar;
    // }

    public function getGenderShowAttribute(){
        $gender_show = '';
        switch($this->getGenderAttribute()){
            case 'male': 
                $gender_show = 'ชาย'; 
                break;
            case 'female': 
                $gender_show = 'หญิง'; 
                break;
            case 'no':
                $gender_show = 'ไม่ระบุ'; 
                break;
        }
        return $gender_show;
    }

    // public function getRoleShowAttribute(){
    //     $role_shows = $this->roles->map(function($vs){
    //         return config('role.names')[$vs['role']];
    //     });
    //     if(empty($role_shows)){
    //         return [];
    //     }
    //     return implode(', ',$role_shows->all());
    // }

    public function getGenderAttribute(){
        if(!empty($this->profiles->gender)){
            return $this->profiles->gender;
        }
    }

    public function getRoleShowAttribute(){
        $role_show = '';
        if(!empty($this->roles)){
            $comma = '';
            foreach($this->roles as $vs){
                $role_show .= $comma.$vs['name_show'];
                $comma = ', ';
            }
        }
        return $role_show;
    }

    public function getStatusShowAttribute(){
        $status = '';
        switch($this->status){
            case 1: 
                $status = 'ปกติ'; 
                break;
            case 0: 
                $status = 'ยกเลิก'; 
                break;
        }
        return $status;
    }

    public function getFirstnameAttribute(){
        if(!empty($this->profiles->firstname)){
            return $this->profiles->firstname;
        }
    }

    public function getLastnameAttribute(){
        if(!empty($this->profiles->lastname)){
            return $this->profiles->lastname;
        }
    }

    public function getPhoneAttribute(){
        if(!empty($this->profiles->phone)){
            return $this->profiles->phone;
        }
    }

    public function getAboutMeAttribute(){
        if(!empty($this->profiles->aboutme)){
            return $this->profiles->aboutme;
        }
    }

    // public function getIsAdminAttribute(){
    //     $count = $this->roles->map(function($vs){
    //         if($vs['role'] = 'admin'){
    //             return 1;
    //         }
    //     })->sum();
    //     if($count > 0){
    //         return true;
    //     }
    //     return false;
    // }

    // public function own($infos){
    //     if(session('user')['id'] != $infos['user_id']){
    //         return 0;	
    //     }
    //     return 1;
    // }

    // public function getStudents(){
    //    // dd($this->with('roles')->get());
    //     // $users = $this->with('roles')->filter(function($vs){		
    //     //     dd($vs);	
    //     // });
    //     return $this->with('profiles')->get()->filter(function($vs){
    //         if($vs->roles()->where('role', 'student')->count() > 0){
    //             return true;
    //         }else{
    //             return false;
    //         }
    //     });
    //     // return $users;
    // }
    
    public function getInfo(){

        $user = $this;

        $user_infos = $user->toArray();

        $user_infos['genderIsSecret'] = 0; 
        $user_infos['genderIsMale'] = 0; 
        $user_infos['genderIsFemale'] = 0; 
        if($user->profiles){
            $user_profiles = $user->profiles->toArray();
            if(!empty($user_profiles)){ 
                switch($user_profiles['gender']){
                    case 'no': 
                        $user_profiles['genderIsSecret'] = 1; 
                        $user_profiles['gender'] = 'ไม่ระบุ'; 
                        break;
                    case 'male': 
                        $user_profiles['genderIsMale'] = 1; 
                        $user_profiles['gender'] = 'ชาย'; 
                        break;
                    case 'female': 
                        $user_profiles['genderIsFemale'] = 1; 
                        $user_profiles['gender'] = 'หญิง'; 
                        break;
                }
                $user_infos = array_merge($user_profiles,$user_infos);
            }
        }

        $user_roles = $user->roles->toArray();
        $user_infos['role_show'] = '';

        foreach(role()->all() as $k => $vs){
            $user_infos['is'.ucfirst($vs['name'])] = 0;
        }

        $user_infos['roles'] = array();
        if(!empty($user_roles)){
            $comma = '';
            foreach($user_roles as $vs){
                $user_infos['is'.ucfirst($vs['name'])] = 1;
                if($user_infos['is'.ucfirst($vs['name'])] == 1){
                    $user_infos['roles'][] = $vs['name'];
                }
                $user_infos['role_show'] .= $comma.$vs['name_show'];
                $comma = ', ';
            }
            $user_roles = array_merge($user_infos,$user_roles);
        }

        switch($user_infos['status']){
            case 1: 
                $user_infos['status_show'] = 'ปกติ'; 
                break;
            case 0: 
                $user_infos['status_show'] = 'ยกเลิก'; 
                break;
        }

        $user_infos['user_id'] = $user_infos['id'];

        return $user_infos;
    }
                
    public function getUsers(){
        $users = \App\Models\User::all()->map(function($user){			
            return $this->getUser($user->id);
        });
        return $users;
    }

    public function getNumberOfUser(){
        $n = \App\Models\User::all()->count();
        return $n;
    }

    public function isLogin(){
        if(!empty(session('user')['id'])){
            if(session('user')['id'] == $this->id){
                return true;
            }
        }
        return false;
    } 

    public function isAdmin(){
        if(!$this->isLogin()){
            return false;
        }else{
            if(empty($this->infos)){
                if(session('user')['isAdmin']){
                    return true;
                }
            }else{
                if($this->infos['isAdmin']){
                    return true;
                }
            }
        }
        return false;
    } 

    public function getSession(){
        if($this->isLogin()){
            return session('user');
        }
        return [];
    }

    // public function id(){
    //     if($this->isLogin()){
    //         return session('user')['id'];
    //     }
    //     return false;
    // }

    // public function withFullInfo($user_id){
    //     $this_user = $this->find($user_id);
    //     $this_user->infos = $this->getInfo($user_id);
    //     return $this_user;
    // }

    public function hasRole($role_name){
        if(empty($this->infos)){
            if($this->isLogin()){
                if(in_array($role_name,session('user')['roles'])){
                    return true;
                }
            }
        }else{
            if(in_array($role_name,$this->infos['roles'])){
                return true;
            }
        }
        return false;
    }

    // public function updateRole(Request $request){
    //     $inputs = $request->all();
    //     unset($inputs['_token']);
    //     $infos = session('user');   

    //     if(user()->isAdmin() && !empty($inputs['user_id'])){
    //         $infos = user($inputs['user_id'])->infos;   
    //     }

    //     if(!empty($inputs['phone'])){
    //         $request->validate(['phone'=>'regex:/^[0-9]*$/i'],['phone.regex'=>'กรุณากรอกเบอร์โทรศัพท์เป็นตัวเลขเท่านั้น']);
    //     }

    //     try{
    //         Profile::updateOrCreate(['user_id'=>$infos['id']],$inputs);
    //     }catch(Throwable $e){
    //         return back()->with(['error'=>'กรุณาลองใหม่อีกครั้ง']);
    //     }

    //     if(user()->id == $infos['id']){
    //         session(['user'=>user()->infos]);
    //     }

    //     return back()->with(['success'=>'ปรับปรุงข้อมูลเรียบร้อย']);
    // }
}
            