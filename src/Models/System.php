<?php

namespace Pongsit\System\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Pongsit\Profile\Models\Profile;
use Pongsit\Role\Models\Role;
use Pongsit\User\Models\User;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Stmt\Return_;

class System extends Model
{
  use HasFactory;

  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  public function roles() {
    return $this->hasMany('Pongsit\Role\Models\Role');
  }

  // public function getUser($user_id=0){
	// 	if(empty($user_id)){
  //     $user = Auth::user();
  //     if(!empty($user)){
  //       $users = User::where('id',$user->id)->first()->toArray();
  //     }else{
  //       return [];
  //     }
	// 	}else{
  //     $users = User::where('id',$user_id)->first()->toArray();
  //   }

	// 	$profile = new Profile();
	// 	$profile_users = $profile->user($users['id']);
  //   if(!empty($profile_users)){
  //     $profiles = $profile_users->toArray();    
  //     switch($profiles['gender']){
  //       case 'no': 
  //         $profiles['gender_no_check'] = 'checked'; 
  //         $profiles['gender'] = 'ไม่ระบุ'; 
  //         break;
  //       case 'male': 
  //         $profiles['gender_male_check'] = 'checked'; 
  //         $profiles['gender'] = 'ชาย'; 
  //         break;
  //       case 'female': 
  //         $profiles['gender_female_check'] = 'checked'; 
  //         $profiles['gender'] = 'หญิง'; 
  //         break;
  //     }
  //     $users = array_merge($profiles,$users);
  //   }

  //   $role = new Role();
	// 	$roles = $role->getUser($users['id']);
  //   $users['role_show'] = '';
  //   if(!empty($roles)){
  //     $role_shows = config('role.names');
  //     $comma = '';
  //     foreach($roles as $v){
  //       $users['is'.ucfirst($v)] = 1;
  //       $users['role_show'] .= $comma.$role_shows[$v];
  //       $comma = ',';
  //     }
  //     $users = array_merge($users,['roles'=>$roles]);
  //   }
  //   $users['user_id'] = $users['id'];
	// 	return $users;
	// }

  public function getNumberOfUser(){
    $n = User::all()->count();
		return $n;
	}

  public function own($infos){
		if(session('user')['id'] != $infos['id']){
			return 0;	
		}
    return 1;
  }
}
