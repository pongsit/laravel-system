<?php

namespace Pongsit\System\Models;
use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    public $timeZone = "Asia/Bangkok";
    
    public function __construct($timeZone="") 
    {
        if(empty($timeZone)){
            $this->set_timezone($this->timeZone);
        }else{
            $this->set_timezone($timeZone);
        }    
    }
    
    function set_timezone($timeZone){
        $this->timeZone = $timeZone;
        date_default_timezone_set($timeZone);
    }
    
    function now($format="Y-m-d H:i:s"){
        $time = time();
        if(strtolower($format)=="unix"){
            return $time;
        }
        if(strtolower($format)=="thai-short"){
            $y = substr(date('Y',$time)+543, -2);
            $m = date('m',$time);
            $d = date('d',$time);
            $m = $this->numToThaiMonth($m);
            return array(0=>$d,'day'=>$d,1=>$m,'month'=>$m,2=>$y,'year'=>$y);
        }
        return date($format,$time);
    }
    function strToThai($yyyy_mm_dd=""){
        if(!empty($yyyy_mm_dd)){
            $explodes = explode(' ', $yyyy_mm_dd);
            $time = $explodes[1];
            $explodes2 = explode('-', $explodes[0]);
            $y = substr($explodes2[0]+543, -2);
            $m = $this->numToThaiMonth($explodes2[1]);
            $d = $explodes2[2];
            return array(0=>$d,'day'=>$d,1=>$m,'month'=>$m,2=>$y,'year'=>$y,2=>$time, 'time'=>$time);
        }
    }
    function thai($ymd){
        $explodes = explode(' ', $ymd);
        $time = '';
        if(!empty($explodes[1])){
            $time = ' '.$explodes[1];
            $dates = explode('-', $explodes[0]);
        }else{
            $dates = explode('-', $ymd);
        }

        $y = substr($dates[0]+543, -2);
        $m = $this->numToThaiMonth($dates[1]);
        $d = $dates[2];
        return $d.' '.$m.$y.$time;
    }
    function numToThaiMonth($m){
        switch($m){
            case 1: $m='ม.ค.'; break;
            case 2: $m='ก.พ.'; break;
            case 3: $m='มี.ค.'; break;
            case 4: $m='เม.ย.'; break;
            case 5: $m='พ.ค.'; break;
            case 6: $m='มิ.ย.'; break;
            case 7: $m='ก.ค.'; break;
            case 8: $m='ส.ค.'; break;
            case 9: $m='ก.ย.'; break;
            case 10: $m='ต.ค.'; break;
            case 11: $m='พ.ย.'; break;
            case 12: $m='ธ.ค.'; break;
        }
        return $m;
    }
    function difference($from, $to){
        $from_time = strtotime($from);
        $to_time = strtotime($to);
        return round(abs($to_time - $from_time) / 60,2); //in minute
    }
    function diff_sec($a, $b){
        $a = strtotime($a);
        $b = strtotime($b);
        return $a - $b;
    }
    function diff($a, $b){
        $a = strtotime($a);
        $b = strtotime($b);
        return $a - $b;
    }
    function shift($amount){ // +365 day
        return date('Y-m-d H:i:s',strtotime($this->now() . $amount));
    }
}