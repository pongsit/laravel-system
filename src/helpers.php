<?php

// run this command: composer update

use Pongsit\User\Models\User;
use Pongsit\Role\Models\Role;
use Illuminate\Support\Facades\Url;
// use Pongsit\Payment\Models\Payment;
use Pongsit\System\Models\Time;
// use Illuminate\Support\Str;

if (! function_exists('user')) {
   function user($user_id=0){ 
      $user = new User;
      if(empty($user_id)){
         if(!empty(session('user'))){
            return session('user');
         }else{
            if(!empty(auth()->user()->id)){
               $user = $user->find(auth()->user()->id);
               session(['user' => $user]);
               return $user;
            }else{
               return $user;
            }
         }
      }else{
         return $user->find($user_id); 
      }
   }
}
if (! function_exists('role')) {
   function role(){ 
      if(empty(cache('role'))){
         $role = new Role;
         cache(['role' => $role]);
         return $role;
      }else{
         return cache('role');
      }
      // if(empty($role_name)){
      //    return $role;
      // }else{
      //    return $role->withFullInfo($role_name); 
      // }
   }
}

if (! function_exists('vela')) {
   function vela(){ 
      $time = new Time;
      return $time;
   }
}

if (! function_exists('unique')) {
   function unique(){ 
      $unique = time().rand(10000,99999);
      return $unique;
   }
}

// if (! function_exists('url')) {
//    function url(){ 
//       return URL;
//    }
// }

// if (! function_exists('payment')) {
//    function payment(){ 
//       return new Payment();
//    }
// }

if (! function_exists('slug')) {
   function slug($model,$attr){ 
     $text = $model->$attr;

     // replace non letter or digits by divider
     $slug = preg_replace("~[^ีืัูุ่้ิํึ์'\pL\d]+~u", '_', $text);

     // trim
     $slug = trim($slug);

     // remove duplicate divider
     // $slug = preg_replace('~-+~', '-', $slug);

     // lowercase
     $slug = strtolower($slug);

     // if (empty($slug)) {
     //   $slug = Str::slug($text, '-', 'th');
     // }

      if(!empty($model->slug)){
         $explodes = explode($slug, $model->slug);
         // ติดปัญหาเวลา update ที่ใช้ slug เดิม พอ update แล้ว slug เปลี่ยน
         // ถ้าแยกออกมาเป็น 2 ส่วนแล้ว คือส่วนของ slug กับ ตัวเลข 
         // แสดงว่าเป็นการ update slug ที่ใช้ slug หลักเดิม จึงให้ใช้ตัวเดิม
         if(count($explodes) == 2){
            return $model->slug;
         }
      }

     if ($model->where('slug',$slug)->exists()) {
         $max = $model->where($attr,$text)->orderBy('slug','desc')->value('slug');
         if(!empty($max)){
            $explodes = explode('-', $max);
            if(count($explodes) == 2){
               if (is_numeric($max[-1])) {  
                    return preg_replace_callback('/(\d+)$/', function ($matches) {
                                return $matches[1] + 1;
                            }, $max);
               }
            }
         }
         return "{$slug}-2";
     }
     return $slug;
   }
}
if (! function_exists('is_support_webp')) {
   function is_support_webp(): bool{
      return isset($_SERVER['HTTP_ACCEPT']) && strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false;
   }
}