<?php

use Illuminate\Support\Facades\Route;
use Pongsit\System\Http\Controllers\SystemController;
use Pongsit\User\Http\Controllers\UserController;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Pongsit\Payment\Http\Controllers\PaymentController;
use Pongsit\Product\Http\Controllers\ProductController;
use Pongsit\Profile\Http\Controllers\ProfileController;
use Pongsit\Role\Http\Controllers\RoleController;
use Pongsit\Payment\Models\Payment;
use Pongsit\System\Http\Controllers\FpController;
use Pongsit\Course\Http\Controllers\CourseController;
use Pongsit\Firebase\Http\Controllers\FirebaseController;
use App\Http\Controllers\Controller;

Auth::routes();

// login
Route::get('/login', [SystemController::class, 'login'])->name('login');
Route::post('/login', [SystemController::class, 'loginFormPost']);
Route::get('/login/form', [SystemController::class, 'loginForm'])->name('login.form');
Route::post('/login/form', [SystemController::class, 'loginFormPost'])->name('login.form');

// socialite
Route::get('/login/{provider}', [SystemController::class, 'getSocialiteProvider']);
Route::get('/callback/{provider}', [SystemController::class, 'getSocialiteCallback']);

Route::middleware(['userCan:manage_role'])->group(function () {
    Route::get('/role/ability/', [RoleController::class, 'roleAbility'])->name('role.ability');
    Route::post('/role/ability/update', [RoleController::class, 'updateAbility'])->name('role.ability.update');
    Route::get('/role/', [RoleController::class, 'index'])->name('role.index');
    Route::post('/role/store', [RoleController::class, 'store'])->name('role.store');
});

Route::get('/register', [SystemController::class, 'register'])->name('register');


Route::group(['prefix' => ''], function () {
    Route::get('', [SystemController::class, 'index'])->name('index');
});

Route::get('/logout/success', [SystemController::class, 'logoutSuccess'])->name('logout.success');
Route::get('/logout/error', [SystemController::class, 'logoutError'])->name('logout.error');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::get('/posts', [PostController::class, 'index'])->name('posts.index');
// Route::get('/posts/{post}', [PostController::class, 'show'])->name('posts.show');
// Route::post('/posts', [PostController::class, 'store'])->name('posts.store');
// Route::middleware(['web'])->group(function () {
    Route::group(['prefix' => 'System'], function () {
        Route::get('', [SystemController::class, 'index']);
    });
// });

Route::get('/dashboard', [Controller::class, 'dashboard'])->name('dashboard');
Route::get('/dashboard/admin', [SystemController::class, 'dashboardAdmin'])->name('dashboard.admin');
// Route::middleware(['userCan:manage_user'])->group(function () {
//     Route::get('/dashboard/{user:slug}', [Controller::class, 'dashboard'])->name('dashboard.slug');
// });

// payment
Route::middleware(['userCan:manage_payment'])->group(function () {
    Route::get('/payment', [PaymentController::class, 'index'])->name('payment');
    Route::get('/payment/user', [PaymentController::class, 'userList'])->name('payment.user');
    Route::post('/payment/update/{payment}', [PaymentController::class, 'update'])->name('payment.update');
    Route::get('/payment/show/refund/{payment:ref2}/{number}', [PaymentController::class, 'showRefund'])->name('payment.showRefund');
    Route::get('/payment/create', [PaymentController::class, 'create'])->name('payment.create');
});
Route::get('/payment/{payment:ref2}', [PaymentController::class, 'show'])->name('payment.show');

// product
Route::middleware(['userCan:manage_product'])->group(function () {   
    Route::post('/product/store/photo/{product:slug}', [ProductController::class, 'storePhoto'])->name('product.store.photo');
    Route::post('/product/reorder/photo/{product:slug}', [ProductController::class, 'reorderPhoto'])->name('product.reorder.photo');
    Route::post('/product/remove/photo/{product:slug}', [ProductController::class, 'removePhoto'])->name('product.remove.photo');
    Route::post('/product/update/{product:slug}', [ProductController::class, 'update'])->name('product.update');
    Route::get('/product/edit/{product:slug}', [ProductController::class, 'edit'])->name('product.edit');
    Route::post('/product/search', [ProductController::class, 'search'])->name('product.search');
    Route::get('/product/create', [ProductController::class, 'create'])->name('product.create');
});
Route::get('/products', [ProductController::class, 'index'])->name('product');
Route::get('/product/{product:slug}', [ProductController::class, 'show'])->name('product.show');
Route::get('/product/show/photo/{product:slug}/{number}/{size}', [ProductController::class, 'showphoto'])->name('product.show.photo');

// filepond
// Route::get('/filepond/api/load/{path}', [ProductController::class, 'filepondLoad'])->name('product.filepond.load');
// Route::delete('/filepond/api/revert', [ProductController::class, 'revertPhoto'])->name('product.filepond.revert');

// course
// Route::get('/course/{course_id}/chapter/{chapter_id}', [CourseController::class, 'show'])->name('course.show');
// Route::get('/course/{course_id}/chapter/{chapter_id}/lesson/{video_id?}', [CourseController::class, 'show'])->name('course.show');










